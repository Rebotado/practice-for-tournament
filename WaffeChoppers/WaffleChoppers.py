from Waffle import Waffle
import random



for T in range(1, 100):
    rows = random.randrange(2,10)
    columns = random.randrange(2,10)
    waffle = Waffle(rows, columns)
    waffle.CutWaffle(random.randrange(0, rows - 1), random.randrange(0, columns - 1))
    print("#" + str(T + 1) + ": " + ("POSSIBLE" if waffle._isPossible() else "IMPOSSIBLE"))