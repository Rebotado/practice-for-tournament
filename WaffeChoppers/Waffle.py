from WaffleCell import WaffleCell
import random

class Waffle:

    _rows = 0
    _columns = 0
    _waffleCells = list()


    _gridUpperLeft = list()
    _gridUpperRight = list()
    _gridLowerLeft = list()
    _gridLowerRight = list()

    def __init__(self, rows, columns):
        self._rows = rows
        self._columns = columns
        self._generateCells()

    def _generateCells(self):
        for row in range(0, self._rows):
            for column in range(0, self._columns):
                self._waffleCells.append(WaffleCell(row, column, random.choice([True, False])))


    def _cutHorizontal(self, pos):
        self._gridUpperLeft = filter(lambda grid: grid._row <= pos, self._waffleCells)
        self._gridLowerLeft = filter(lambda grid: grid._row > pos, self._waffleCells)

    def _cutVertical(self, pos):
        self._gridUpperRight = filter(lambda grid: grid._column > pos, self._gridUpperLeft)
        self._gridLowerRight = filter(lambda grid: grid._column > pos, self._gridLowerLeft)
        self._gridUpperLeft = filter(lambda grid: grid._column <= pos, self._gridUpperLeft)
        self._gridLowerLeft = filter(lambda grid: grid._column <= pos, self._gridLowerLeft)

        
    def CutWaffle(self, posRow, posColumn):
        self._cutHorizontal(posRow)
        self._cutVertical(posColumn)


    def _isPossible(self):
        if (self._chocolateCount(self._gridUpperLeft) == self._chocolateCount(self._gridUpperRight)
        and self._chocolateCount(self._gridUpperRight) == self._chocolateCount(self._gridLowerLeft)
        and self._chocolateCount(self._gridLowerLeft) == self._chocolateCount(self._gridLowerRight)):
            return True
        else:
            return False
         

    def _chocolateCount(self, grid):
        waffles = filter(lambda x: x._hasChocolate == True, grid)
        return len(waffles)


    def printWaffles(self):
        for waffle in self._waffleCells:
            print(waffle)

    def printLowerLeft(self):
        for waffle in self._gridLowerLeft:
            print(waffle)

    def printLowerRight(self):
        for waffle in self._gridLowerRight:
            print(waffle)

    def printUpperLeft(self):
        for waffle in self._gridUpperLeft:
            print(waffle)
    
    def printUpperRight(self):
        for waffle in self._gridUpperRight:
            print(waffle)