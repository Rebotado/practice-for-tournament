class WaffleCell:

    _row = 0
    _column = 0
    _hasChocolate = True

    def __init__(self, row, column, hasChocolate):
        self._row = row
        self._column = column
        self._hasChocolate = hasChocolate

    def __str__(self):
        return "row: {0} column: {1} Has Chocolate: {2}".format(self._row, self._column, self._hasChocolate)
